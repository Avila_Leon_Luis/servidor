/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author gmendez
 */
public class Servidor {
    
    ArrayList<Conexion> conexiones;
    ServerSocket ss;
    Connection conn;
    ArrayList<String> usuarios;
    
    
    public static void main(String args[]) {
       
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        }); 
    }

    private void start() {
        this.conexiones = new ArrayList<>();
        this.usuarios = new ArrayList<>();
        Socket socket;
        Conexion cnx;
        
        try {
            ss = new ServerSocket(4000);//Genero el Server
            System.out.println("Servidor iniciado, en espera de conexiones");

            while (true){//Crean los hilos
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size());
                conexiones.add(cnx);
                cnx.start();                
            }            
                       
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){
                if (!id.equals(hilo.id)){
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;        
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";
        Statement statement;
        
        public final int SIN_USER   = 0;
        public final int USER_IDENT = 1;
        public final int PASS_PDTE  = 2;
        public final int PASS_OK    = 3;
        public final int CHAT       = 4;
                
        public Conexion(Servidor padre, Socket socket, int num){
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;
        }
        
        @Override
        public void run() {
            String user="", pass="", mensaje="",SQuerry,nombre="",password = "";
            int estado = SIN_USER;
            ResultSet datos = null;
            
            try {
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/usuarios","root","K91gb7mp8d0#");
                    statement = conn.createStatement();
                } catch (SQLException ex) {
                    Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                }
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());            
                while(!mensaje.toLowerCase().equals("salir")){                    
                    switch (estado){
                        case SIN_USER:
                                out.println("Bienvenido, proporcione su usuario");
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readLine();
                            boolean found = false;
                            SQuerry = "select nombre,contrasena from usuarios where nombre = '"+user+"';";
                            try{
                                datos = statement.executeQuery(SQuerry);
                                if(datos.next()){
                                    nombre = datos.getString("nombre");
                                    password = datos.getString("contrasena");
                                    if(user.equals(nombre)){
                                        found = true;
                                        usuarios.add(nombre);
                                    }
                            }
                            } catch (SQLException ex) {
                            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                           
                            if (!found){
                                estado = SIN_USER;
                            } else {
                                estado = PASS_PDTE;
                            }                  
                            break;

                        case PASS_PDTE:
                            out.println("Escriba el password");
                            pass = in.readLine();                            
                            found = false;
                            
                            if(pass.equals(password)){
                                found = true;
                            
                            try{
                                conn.close();
                                conn = null;
                                datos.close();
                                datos  = null;
                                statement.close();
                                statement = null;
                                
                            }catch(SQLException ex){
                                System.out.println(ex);
                            }
                            }
                                    
                            if (!found){
                                estado = PASS_PDTE;
                                
                            } else {
                                estado = PASS_OK;
                            }
                            
                            break;


                        case PASS_OK:
                            out.println("Autenticado!");
                            this.padre.difundir(this.id,user+" Se ha unido a la conversacion.");
                            out.println(usuarios);
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readLine();
                            if (mensaje.equalsIgnoreCase("salir")) {
                                break;
                            }
                            System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ": " +mensaje);
                            
                            this.padre.difundir(this.id, user+" : "+mensaje);
                            break;
                    }                        
                }
                difundir(this.id,user+" ha salido del chat.");
                usuarios.remove(user);
                this.cnx.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        } 
    }
    
    public String usersToString(){
        String strUsers = "";
        for (int i = 0; i < usuarios.size();i++) {
            strUsers = strUsers + usuarios.get(i) + ",";
        }
        return strUsers;
    }
}
